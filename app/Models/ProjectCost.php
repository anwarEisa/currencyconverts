<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectCost extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id',
        'currency_id',
        'cost',
    ];

    public function project(){
        return $this->belongsTo('App\Models\Project','project_id');

    }
    public function currency(){
        return $this->belongsTo('App\Models\Currency','currency_id');

    }
}
