<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConvertCurrencyRequest;
use App\Http\Requests\CostByDefaultCurrencyRequest;
use App\Http\Requests\CostByTargetCurrencyRequest;
use App\Models\Currency;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    //

    public function calculateProjectCostBySpecificCurrency(CostByTargetCurrencyRequest $request)
    {
        $project=Project::with('cost')->find($request->project_id);
        if($project==null){
            return response([
                'status' => 'Failed',
                'data'=>'null',
                'message'=>'Please enter correct project id'
            ]);
        }
        $valueByDefaultCurrency=$this->convertToDefaultCurrency($project->cost[0]['currency_id'],$project->cost[0]['cost']);
       if($valueByDefaultCurrency==false){
           return response([
               'status' => 'Failed',
               'data'=>'null',
               'message'=>'Please enter correct currency id'
           ]);
       }
        $res['valueByTargetCurrency']=$this->convertFromDefaultCurrency($request->target_currency_id,$valueByDefaultCurrency);

        if($res['valueByTargetCurrency']==false){
            return response([
                'status' => 'Failed',
                'data'=>'null',
                'message'=>'Please enter correct currency id'
            ]);
        }

        return response([
            'status' => 'success',
            'data'=>$res,
            'message'=>'we convert it successfully'
        ]);
    }
    public function convertCurrencyToDefault(CostByDefaultCurrencyRequest $request)
    {
        $res['valueByDefaultCurrency']=$this->convertToDefaultCurrency($request->current_currency_id,$request->value);
        if($res['valueByDefaultCurrency']==false){
            return response([
                'status' => 'Failed',
                'data'=>'null',
                'message'=>'Please enter correct currency id'
            ]);
        }
        return response([
            'status' => 'success',
            'data'=>$res,
            'message'=>'we convert it successfully'
        ]);
    }
    public function convertCurrency(ConvertCurrencyRequest $request)
    {
        $valueByDefaultCurrency=$this->convertToDefaultCurrency($request->current_currency_id,$request->value);
        if($valueByDefaultCurrency==false){
            return response([
                'status' => 'Failed',
                'data'=>'null',
                'message'=>'Please enter correct currency id'
            ]);
        }
        $res['valueByTargetCurrency']=$this->convertFromDefaultCurrency($request->target_currency_id,$valueByDefaultCurrency);
        if($res['valueByTargetCurrency']==false){
            return response([
                'status' => 'Failed',
                'data'=>'null',
                'message'=>'Please enter correct currency id'
            ]);
        }
        return response([
            'status' => 'success',
            'data'=>$res,
            'message'=>'we convert it successfully'
        ]);

    }

    public function convertToDefaultCurrency($currency_id,$value)
    {
        $currency=Currency::find($currency_id);
        if($currency==null){
            return false;
        }
        return $value/$currency->value;
    }
    public function convertFromDefaultCurrency($currency_id,$value)
    {
        $currency=Currency::find($currency_id);
        if($currency==null){
            return false;
        }
        return $value*$currency->value;
    }
}
