<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ConvertToDefaultCurrencyTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_convert_to_default_currency(): void
    {


        $data = [
            'current_currency_id' => 2,
            'value' => 100000,
        ];

        $response = $this->postJson('/api/convertCurrencyToDefault', $data);
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'data' => [
                    "valueByDefaultCurrency"=> 1000,
                ],
                'message' => 'we convert it successfully',
            ]);
    }
}
