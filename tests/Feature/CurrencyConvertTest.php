<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CurrencyConvertTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_currency_convert(): void
    {


        $data = [
            'current_currency_id' => 1,
            'target_currency_id' => 2,
            'value' => 100000,
        ];

        $response = $this->postJson('/api/convertCurrency', $data);
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'data' => [
                    'valueByTargetCurrency' => 1000000,
                ],
                'message' => 'we convert it successfully',
            ]);
    }
}
