<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CalculateProjectCostBySpecificCurrencyTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_calculate_project_costBy_specific_currency(): void
    {
        $data = [
            'project_id' => 1,
            'target_currency_id' => 2,
        ];

        $response = $this->postJson('/api/calculateProjectCostBySpecificCurrency', $data);
        $response->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'data' => [
                    "valueByTargetCurrency"=> 1000000,
                ],
                'message' => 'we convert it successfully',
            ]);
    }

}
